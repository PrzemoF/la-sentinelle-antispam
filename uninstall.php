<?php
/*
 * This file will be called when pressing 'Delete' on Dashboard > Plugins.
 */


// if uninstall.php is not called by WordPress, die.
if ( ! defined('WP_UNINSTALL_PLUGIN') ) {
    die();
}

$option_names = array(
		'la_sentinelle-honeypot',
		'la_sentinelle-nonce',
		'la_sentinelle-timeout',
		'la_sentinelle-wpcomment',
		'la_sentinelle-wplogin',
		'la_sentinelle-wppassword',
		'la_sentinelle-wpregister',
		'la_sentinelle-formidable',
		'la_sentinelle-save_comments',
		'la_sentinelle-remove_comments',
		'la_sentinelle-honeypot_value',
		'la_sentinelle-version'
	);

foreach ( $option_names as $option_name ) {

	delete_option( $option_name );

	// for site options in Multisite
	delete_site_option( $option_name );

}
