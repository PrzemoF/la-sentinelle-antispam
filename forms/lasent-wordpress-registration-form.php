<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Add spamfilter fields to registration form.
 *
 * @since 1.0.0
 */
function la_sentinelle_registration_form() {

	echo la_sentinelle_get_spamfilters();

}
if (get_option( 'la_sentinelle-wpregister', 'true') == 'true') {

	// Single site:
	add_action( 'register_form', 'la_sentinelle_registration_form' );
	add_action( 'register_form', 'la_sentinelle_dead_enqueue' );

	// MultiSite:
	add_action( 'signup_extra_fields', 'la_sentinelle_registration_form' );
	add_action( 'signup_extra_fields', 'la_sentinelle_dead_enqueue' );

	// EDD shortcode [edd_register]
	add_action( 'edd_register_form_fields_before', 'la_sentinelle_registration_form' );

}


/*
 * Check fields in registration form and return errors if needed.
 * Adds integration to any core WordPress registration form, like the one at /wp-login.php?action=register
 */
function la_sentinelle_check_registration_form( $errors, $sanitized_user_login, $user_email ) {

	$marker = la_sentinelle_check_spamfilters();
	if ( $marker == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'wpregister' );
		$errors->add( 'likely_spammer', esc_html__( 'Your registration was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}

	return $errors;
}
if (get_option( 'la_sentinelle-wpregister', 'true') == 'true') {
	add_filter( 'registration_errors', 'la_sentinelle_check_registration_form', 10, 3 );
}


/*
 * Check fields in registration form and return errors if needed.
 * Adds integration to any core WordPress registration form, like the one at /wp-login.php?action=register
 */
function la_sentinelle_check_registration_form_mu( $errors, $sanitized_user_login, $user_email ) {

	$marker = la_sentinelle_check_spamfilters();
	if ( $marker == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'wpregister' );
		empty($results['errors']) || ! is_wp_error($results['errors']) ? $results['errors'] = new WP_Error() : null;
		$errors['errors']->add( 'likely_spammer', esc_html__( 'Your registration was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}

	return $errors;
}
if (get_option( 'la_sentinelle-wpregister', 'true') == 'true') {
	add_filter( 'wpmu_validate_user_signup', 'la_sentinelle_check_registration_form_mu', 10, 3 );
}


/*
 * Adds integration with Restrict Content Pro
 * @url https://restrictcontentpro.com/
 */
function la_sentinelle_check_registration_form_rcp( $user ) {

	$marker = la_sentinelle_check_spamfilters();
	if ( $marker == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'wpregister' );
		rcp_errors()->add( 'likely_spammer', esc_html__( 'Your registration was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}

	return $user;
}
if (get_option( 'la_sentinelle-wpregister', 'true') == 'true') {
	add_filter( 'rcp_user_registration_data', 'la_sentinelle_check_registration_form_rcp' );
}


/*
 * Adds integration with MemberPress
 * @url https://www.memberpress.com/
 */
function la_sentinelle_check_registration_form_mepr( $errors ) {

	$marker = la_sentinelle_check_spamfilters();
	if ( $marker == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'wpregister' );
		$errors[] = esc_html__( 'Your registration was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' );
	}

	return $errors;
}
if (get_option( 'la_sentinelle-wpregister', 'true') == 'true') {
	add_filter( 'mepr-validate-signup', 'la_sentinelle_check_registration_form_mepr' );
}


/*
 * Adds integration with Give
 * @url https://givewp.com/
 */
function la_sentinelle_check_registration_form_give() {

	$marker = la_sentinelle_check_spamfilters();
	if ( $marker == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'wpregister' );
		give_set_error( 'likely_spammer', esc_html__( 'Your registration was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}

}
if (get_option( 'la_sentinelle-wpregister', 'true') == 'true') {
	add_action( 'give_pre_process_register_form', 'la_sentinelle_check_registration_form_give' );
}


/*
 * Check fields in EDD register form.
 *
 * @since 1.5.0
 */
function la_sentinelle_check_registration_form_edd() {

	if ( ! function_exists( 'edd_set_error' ) ) {
		return;
	}

	if ( defined('XMLRPC_REQUEST') && XMLRPC_REQUEST ) {
		return;
	}

	$marker_nonce = la_sentinelle_check_nonce();
	if ( $marker_nonce == 'spam' ) {
		edd_set_error( 'likely_spammer', esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}
	$marker_honeypot = la_sentinelle_check_honeypot();
	if ( $marker_honeypot == 'spam' ) {
		edd_set_error( 'likely_spammer', esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}
	$marker_timeout = la_sentinelle_check_timeout();
	if ( $marker_timeout == 'spam' ) {
		edd_set_error( 'likely_spammer', esc_html__( 'Your submission was sent in too fast. Please slow down and try again.', 'la-sentinelle-antispam' ) );
	}

	if ( $marker_nonce == 'spam' || $marker_honeypot == 'spam' || $marker_timeout == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'wpregister' );
		// Do a redirect and do not process any further. EDD will log the user in otherwise. The error gets shown after the redirect.
		$redirect = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		wp_safe_redirect( $redirect );
		exit;
	}

}
if (get_option( 'la_sentinelle-wpregister', 'true') == 'true') {
	add_action( 'edd_pre_process_register_form', 'la_sentinelle_check_registration_form_edd' );
}
