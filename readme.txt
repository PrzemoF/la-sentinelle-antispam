=== La Sentinelle antispam ===
Contributors: mpol
Tags: antispam, anti-spam, comments, registration, forms
Requires at least: 3.7
Tested up to: 5.4
Stable tag: 1.5.3
License: GPLv2 or later

Feel safe knowing that your website is safe from spam. La Sentinelle will guard your WordPress website against spam in a simple and effective way.

== Description ==

Feel safe knowing that your website is safe from spam. La Sentinelle will guard your WordPress website against spam in a simple and effective way.
It has antispam filters for comment forms and registration forms and can be extended to support plugins.
The default settings should catch most spambots, and there is a settingspage to set it up according to your wishes.


Current features include:

* 3 antispam features; Honeypot, Nonce, Form Timeout.
* Spamfilters depend on JavaScript on the frontend.
* Settingspage to set things up according to your wishes.
* Lightweight and simple code.
* Transparent to the visitor, no nagging with Captcha's or other annoying things.
* No use of third-party services and no tracking of visitors.
* Logging for WordPress Comments and which spamfilter marked it as spam.


WordPress forms that are protected:

* WordPress Comments form.
* WordPress Login form.
* WordPress Register form.
* WordPress Lost Password form.

Form Plugins that are protected:

* [Caldera Forms](https://wordpress.org/plugins/caldera-forms/).
* [Contact Form 7](https://wordpress.org/plugins/contact-form-7/).
* [Easy Digital Downloads](https://wordpress.org/plugins/easy-digital-downloads/) (Login form, Register form).
* [Formidable Form Builder](https://wordpress.org/plugins/formidable/).
* [WooCommerce](https://wordpress.org/plugins/woocommerce/) (Login form, Lost Password form).
* [Clean Login](https://wordpress.org/plugins/clean-login/) (Login form).


= Support =

If you have a problem or a feature request, please post it on the plugin's support forum on [wordpress.org](https://wordpress.org/support/plugin/la-sentinelle-antispam). I will do my best to respond as soon as possible.

If you send me an email, I will not reply. Please use the support forum.

= Translations =

Translations can be added very easily through [GlotPress](https://translate.wordpress.org/projects/wp-plugins/la-sentinelle-antispam).
You can start translating strings there for your locale. They need to be validated though, so if there's no validator yet, and you want to apply for being validator (PTE), please post it on the support forum.
I will make a request on make/polyglots to have you added as validator for this plugin/locale.

= How to choose an antispam plugin =

When you look through the WordPress Plugin Repository you will see more than a hundred antispam plugins.
Which one is the best one? Short answer, there is no "best one". No spamfilter and no method for spamfiltering is perfect.
Slightly longer answer, you could try about twenty and choose the one that fits your needs best.

But there is also a really long answer.
There are different methods that can be used against spam, and every method has its drawbacks.

* Third party services: Services like Akismet, Stop Forum Spam and also reCAPTCHA offer third party services to check for spam. This can be very effective, but you are giving user submitted data away to these third parties and are also giving your users up for tracking them.
* Captcha's, reCAPTCHA and Quizz Questions: You are annoying your users. This also counts for reCAPTCHA for visitors who have third party cookies disabled.
* Blacklists: Often running behind the facts. That goes for the way of getting users off that list, and also in getting users on that list.
* Referrer check: check if the Referer header is set correctly. You can never trust it is set correctly.
* JavaScript methods: Spammers often (always?) don't use JavaScript, they just post the form with spammy data. Drawback is that statistics say that about 1 percent of users has JavaScript disabled. Also, some websites have broken JavaScript, which will make the spamfilter break as well.
* Activation email for registering users. Users only get activated after clicking a link in an activation email. You still have all the non-activated users in your site however.

You could have a bright idea about combining several methods, but then you get the drawbacks of all the methods you use.

Another complication of choosing a good plugin is that most antispam plugins don't tell you what methods they use. The documentation doesn't tell you, and looking at the source code just leaves you confused at the chaos that it is.

My main motivation for writing this plugin is to offer a plugin that does spamfiltering with JavaScript methods in a simple and effective way.
The claimed 1 percent of users that has JavaScript disabled will also be tech-savy enough to enable it again for your website.

= Compatibility =

This plugin is compatible with [ClassicPress](https://www.classicpress.net).


== Installation ==

= Installation =

* Install the plugin through the admin page "Plugins".
* Alternatively, unpack and upload the contents of the zipfile to your '/wp-content/plugins/' directory.
* Activate the plugin through the 'Plugins' menu in WordPress.
* That's it.

= License =

The plugin itself is released under the GNU General Public License. A copy of this license can be found at the license homepage or in the la-sentinelle.php file at the top.


== Frequently Asked Questions ==

= I get false positives =

It could be that you have a JavaScript error. That way the spamfilters won't work and all messages get flagged as spam.
You could go to your form page, right click on the page, select "Inspect Element" > Console-tab. Reload the page and see if there are errors in your console.

= I am being targeted by a spammer =

That is unfortunate. I advise to add an extra plugin in the form of Akismet, that should provide a good defense against targeted attacks.


== Screenshots ==

1. Settings page with the spamfilters that are enabled by default.
2. Settings page with the forms for which the spamfilters are enabled.
3. Settings page with extra options.
4. Settings page with statistics about the spam that was blocked or cought together with support links.


== Changelog ==

= 1.5.3 =
* 2020-04-14
* Run timeout function only once.
* Remove function 'la_sentinelle_timout_clock'.
* Add uninstall.php file to uninstall options from db.

= 1.5.2 =
* 2020-02-23
* Use classes, not ids for input fields.
* Add support for Clean Login (only login form for now).
* Set Nonce by default as disabled.
* Support new wp_initialize_site action for multisite.

= 1.5.1 =
* 2019-03-09
* Rewrite error messages for Contact Form 7.
* Use esc_html functions everywhere.

= 1.5.0 =
* 2019-02-07
* Add support for Caldera Forms plugin.
* Add support for Easy Digital Downloads plugin.
* Add support for Formidable plugin.
* Remove unneeded option to enable for Contact Form 7.
* Rewrite functions for statistics.
* Only show statistics for active plugin.

= 1.4.1 =
* 2019-01-08
* Add some accessibility fixes.
* Don't use transients for hashed field names, is faster this way.
* Add review link to admin page.

= 1.4.0 =
* 2018-12-20
* Fix register form.
* Add more statistics.
* Refactor WooCommerce hooks into WordPress hooks.

= 1.3.1 =
* 2018-12-20
* Add counter for comments blocked.

= 1.3.0 =
* 2018-12-19
* Add option to save or not save spam comments (default is save).
* Change timeout from 3s to 1s.
* Only load admin code on admin pages.

= 1.2.1 =
* 2018-11-20
* Fix admin login.

= 1.2.0 =
* 2018-11-16
* Add settings for each supported form.
* Rewrite JavaScript to support multiple forms.
* Always enqueue JavaScript.

= 1.1.0 =
* 2018-08-29
* Support WooCommerce forms (Login form, Lost Password form).
* Add log as comment_meta for comment form.
* Add 'li' for spam comments to dashboard widget 'Right Now'.
* Remove 'la_sentinelle_preprocess_comment' function, was unused.

= 1.0.2 =
* 2018-08-22
* Add option to remove spam comments after 3 months.

= 1.0.1 =
* 2018-08-04
* Show text when no supported plugins are installed.
* Show correct message on login form for timeout.
* Change timeout from 5s to 3s.

= 1.0.0 =
* 2018-07-27
* Initial release
