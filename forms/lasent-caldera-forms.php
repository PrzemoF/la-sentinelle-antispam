<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


function la_sentinelle_caldera_forms_init(){
	$la_sentinelle = new CF_La_Sentinelle();

	add_filter( 'caldera_forms_get_field_types', array( $la_sentinelle, 'add_field' ), 25 );

	add_filter( 'caldera_forms_validate_field_la_sentinelle', array( $la_sentinelle, 'check_spamfilters' ), 10, 3 );
}
add_action( 'caldera_forms_includes_complete', 'la_sentinelle_caldera_forms_init' );




class CF_La_Sentinelle {


	/*
	 * Add field in Caldera Forms
	 *
	 * @since 1.5.0
	 *
	 * @uses "caldera_forms_get_field_types" filter
	 *
	 * @param array $fields Registered fields
	 *
	 * @return array
	 */
	public function add_field( $fields ){
		$fields['la_sentinelle']      = array(
			"field"       => esc_html__( 'La Sentinelle', 'la-sentinelle-antispam' ),
			"description" => esc_html__( 'La Sentinelle antispam', 'la-sentinelle-antispam' ),
			"file"        => LASENT_DIR . '/forms/caldera-forms/field.php',
			"category"    => esc_html__( 'Special', 'caldera-forms' ),
			"handler"     => array( $this, 'handler' ),
			"capture"     => false,
			"setup"       => array(
				"template"      => LASENT_DIR . '/forms/caldera-forms/config.php',
				"preview"       => LASENT_DIR . '/forms/caldera-forms/preview.php',
				"not_supported" => array(
					'hide_label',
					'caption',
					'required'
				),
			),
			"scripts" => array()
		);

		return $fields;
	}


	/*
	 * Field handler -- checks for la_sentinelle and verifies it.
	 *
	 * @since 1.5.0
	 *
	 * @param string $value Field value, should be empty
	 * @param array $field Field config
	 * @param array $form Form config
	 *
	 * @return WP_Error|boolean
	 */
	public function handler( $value, $field, $form ) {

		$marker_nonce = la_sentinelle_check_nonce();
		if ( $marker_nonce == 'spam' ) {
			la_sentinelle_add_statistic_blocked( 'caldera' );
			return new WP_Error( 'error',
				esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' )
			);
		}
		$marker_honeypot = la_sentinelle_check_honeypot();
		if ( $marker_honeypot == 'spam' ) {
			la_sentinelle_add_statistic_blocked( 'caldera' );
			return new WP_Error( 'error',
				esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' )
			);
		}
		$marker_timeout = la_sentinelle_check_timeout();
		if ( $marker_timeout == 'spam' ) {
			la_sentinelle_add_statistic_blocked( 'caldera' );
			return new WP_Error( 'error',
				esc_html__( 'Your submission was sent in too fast. Please slow down and try again.', 'la-sentinelle-antispam' )
			);
		}

		// Do not return a value, that would come into the email content.
		return;

	}

}
