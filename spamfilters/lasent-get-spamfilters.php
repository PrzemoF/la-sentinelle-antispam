<?php

// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Get form fields for a form.
 *
 * @return string html with enabled input fields.
 *
 * @since 1.0.0
 */
function la_sentinelle_get_spamfilters( $nonce_action = 'default' ) {

	$output = '';

	$filters = range(1, 4);
	shuffle( $filters ); // random order.
	foreach ( $filters as $filter ) {
		if ( $filter == 1 ) {
			$output .= la_sentinelle_get_nonce( $nonce_action );
		} else if ( $filter == 2 ) {
			$output .= la_sentinelle_get_honeypot( true );
		} else if ( $filter == 3 ) {
			$output .= la_sentinelle_get_honeypot( false );
		} else if ( $filter == 4 ) {
			$output .= la_sentinelle_get_timeout();
		}
	}

	$output .= '<noscript><div class="no-js">' . esc_html__( 'Warning: This form can only be used if JavaScript is enabled in your browser.', 'la-sentinelle-antispam' ) . '</div></noscript>';

	return $output;
}


/*
 * Enqueue script.
 * Load it on admin_enqueue as well.
 *
 * @since 1.0.0
 */
function la_sentinelle_enqueue() {

	wp_register_script( 'la_sentinelle_frontend_js', plugins_url('js/la-sentinelle-frontend.js', __FILE__), 'jquery', LASENT_VER, true );
	$dataToBePassed = array(
		'honeypot'     => la_sentinelle_get_field_name( 'honeypot' ),
		'honeypot2'    => la_sentinelle_get_field_name( 'honeypot2' ),
		'timeout'      => la_sentinelle_get_field_name( 'timeout' ),
		'timeout2'     => la_sentinelle_get_field_name( 'timeout2' )
	);
	wp_localize_script( 'la_sentinelle_frontend_js', 'la_sentinelle_frontend_script', $dataToBePassed );
	wp_enqueue_script('la_sentinelle_frontend_js');

}
add_action( 'wp_enqueue_scripts', 'la_sentinelle_enqueue' );
add_action( 'admin_enqueue_scripts', 'la_sentinelle_enqueue' );


/*
 * Load styles and scripts the oldfashioned way.
 * Add spamfilter fields to login_form and other silly forms without a proper hook.
 *
 * @since 1.0.0
 */
function la_sentinelle_dead_enqueue() {

	$url = get_bloginfo('wpurl') . '/wp-includes/js/jquery/jquery.js' . '?ver=' . LASENT_VER;
	echo "<script type='text/javascript' src='" . $url . "'></script>";

	$url =  LASENT_URL . 'spamfilters/js/la-sentinelle-frontend.js' . '?ver=' . LASENT_VER;
	echo "<script type='text/javascript' src='" . $url . "'></script>";

	?>
	<script type='text/javascript'>
	var la_sentinelle_frontend_script = {
		"honeypot": "<?php echo la_sentinelle_get_field_name( 'honeypot' ); ?>",
		"honeypot2":"<?php echo la_sentinelle_get_field_name( 'honeypot2' ); ?>",
		"timeout":  "<?php echo la_sentinelle_get_field_name( 'timeout' ); ?>",
		"timeout2": "<?php echo la_sentinelle_get_field_name( 'timeout2' ); ?>"
	};
	</script>
	<?php
}
