<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}



/*
 * Add field in Formidable
 *
 * @since 1.5.0
 *
 * @uses "frm_entry_form" action
 *
 * @param array $form html with the form.
 *
 * @return string html with the input fields.
 */
function la_sentinelle_frm_entry_form($form, $action='', $errors=''){

	echo la_sentinelle_get_spamfilters();

}
if (get_option( 'la_sentinelle-formidable', 'true') == 'true') {
	add_action('frm_entry_form', 'la_sentinelle_frm_entry_form', 10, 3);
}


/*
 * Validate form in Formidable
 *
 * @since 1.5.0
 *
 * @uses "frm_validate_entry" action
 *
 * @param array $errors
 *
 * @return array $errors
 */
function la_sentinelle_frm_validate_entry($errors, $values=''){

	$marker_nonce = la_sentinelle_check_nonce();
	if ( $marker_nonce == 'spam' ) {
		$errors['la_sentinelle'] = esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' );
	}
	$marker_honeypot = la_sentinelle_check_honeypot();
	if ( $marker_honeypot == 'spam' ) {
		$errors['la_sentinelle'] = esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' );
	}
	$marker_timeout = la_sentinelle_check_timeout();
	if ( $marker_timeout == 'spam' ) {
		$errors['la_sentinelle'] = esc_html__( 'Your submission was sent in too fast. Please slow down and try again.', 'la-sentinelle-antispam' );
	}

	if ( $marker_nonce == 'spam' || $marker_honeypot == 'spam' || $marker_timeout == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'formidable' );
	}

	return $errors;

}
if (get_option( 'la_sentinelle-formidable', 'true') == 'true') {
	add_filter('frm_validate_entry', 'la_sentinelle_frm_validate_entry', 8, 2);
}
