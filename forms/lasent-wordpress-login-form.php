<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Add spamfilter fields to login form.
 *
 * @since 1.0.0
 */
function la_sentinelle_login_form() {

	echo la_sentinelle_get_spamfilters();

}
if (get_option( 'la_sentinelle-wplogin', 'true') == 'true') {
	// Add spamfilter fields to WordPress login form.
	add_action( 'login_form', 'la_sentinelle_login_form' );
	add_action( 'login_form', 'la_sentinelle_dead_enqueue' );

	// Add spamfilter fields to WooCommerce login form.
	add_action( 'woocommerce_login_form', 'la_sentinelle_login_form' );

	// Add spamfilter fields to EDD login form [edd_login].
	add_action( 'edd_login_fields_before', 'la_sentinelle_login_form' );

	// Add spamfilter fields to Clean login form [clean-login].
	add_action( 'cl_login_form', 'la_sentinelle_login_form' );
}


/*
 * Check fields in login form.
 *
 * @param array errors
 *
 * @since 1.0.0
 */
function la_sentinelle_authenticate( $errors ) {

	if ( defined('XMLRPC_REQUEST') && XMLRPC_REQUEST ) {
		return;
	}

	$marker_nonce = la_sentinelle_check_nonce();
	if ( $marker_nonce == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'wplogin' );
		return new WP_Error( 'likely_spammer', esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}
	$marker_honeypot = la_sentinelle_check_honeypot();
	if ( $marker_honeypot == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'wplogin' );
		return new WP_Error( 'likely_spammer', esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}
	$marker_timeout = la_sentinelle_check_timeout();
	if ( $marker_timeout == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'wplogin' );
		return new WP_Error( 'likely_spammer', esc_html__( 'Your submission was sent in too fast. Please slow down and try again.', 'la-sentinelle-antispam' ) );
	}

	return $errors;

}
if (get_option( 'la_sentinelle-wplogin', 'true') == 'true') {
	// Check fields in WordPress login form.
	add_filter( 'wp_authenticate_user', 'la_sentinelle_authenticate', 10, 1 );

	// Check fields in WooCommerce login form.
	add_filter( 'woocommerce_process_registration_errors', 'la_sentinelle_authenticate', 10, 1 );
}


/*
 * Check fields in EDD login form.
 *
 * @param array data
 *
 * @since 1.5.0
 */
function la_sentinelle_edd_authenticate( $data ) {

	if ( ! function_exists( 'edd_set_error' ) ) {
		return;
	}

	if ( defined('XMLRPC_REQUEST') && XMLRPC_REQUEST ) {
		return;
	}

	$marker_nonce = la_sentinelle_check_nonce();
	if ( $marker_nonce == 'spam' ) {
		edd_set_error( 'likely_spammer', esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}
	$marker_honeypot = la_sentinelle_check_honeypot();
	if ( $marker_honeypot == 'spam' ) {
		edd_set_error( 'likely_spammer', esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}
	$marker_timeout = la_sentinelle_check_timeout();
	if ( $marker_timeout == 'spam' ) {
		edd_set_error( 'likely_spammer', esc_html__( 'Your submission was sent in too fast. Please slow down and try again.', 'la-sentinelle-antispam' ) );
	}

	if ( $marker_nonce == 'spam' || $marker_honeypot == 'spam' || $marker_timeout == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'wplogin' );
		// Do a redirect and do not process any further. EDD will log the user in otherwise. The error gets shown after the redirect.
		$redirect = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		wp_safe_redirect( $redirect );
		exit;
	}

}
if (get_option( 'la_sentinelle-wplogin', 'true') == 'true') {
	add_action( 'edd_user_login', 'la_sentinelle_edd_authenticate', 1 );
}
