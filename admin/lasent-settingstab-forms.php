<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Forms tab of the Settings page.
 */
function la_sentinelle_settingstab_forms() {

	if ( function_exists('current_user_can') && ! current_user_can('manage_options') ) {
		die(esc_html__('You need a higher level of permission.', 'la-sentinelle-antispam'));
	} ?>

	<input type="hidden" id="la_sentinelle_tab" name="la_sentinelle_tab" value="la_sentinelle_settingstab_forms" />
	<?php
	settings_fields( 'la_sentinelle_options' );
	do_settings_sections( 'la_sentinelle_options' );

	/* Nonce */
	$nonce = wp_create_nonce( 'la_sentinelle_settingstab_forms' );
	echo '<input type="hidden" id="la_sentinelle_settingstab_forms" name="la_sentinelle_settingstab_forms" value="' . $nonce . '" />';
	?>
	<table class="form-table">
		<tbody>

			<tr valign="top">
				<th scope="row">
					WordPress
				</th>
				<td>
					<input <?php
					if (get_option( 'la_sentinelle-wpcomment', 'true') == 'true') {
						echo 'checked="checked"';
					} ?>
					type="checkbox" name="la_sentinelle-wpcomment" id="la_sentinelle-wpcomment">
					<label for="la_sentinelle-wpcomment">
						<?php esc_html_e('Enable spamfilters for WordPress Comment Forms.', 'la-sentinelle-antispam'); ?>
					</label><br /><br />

					<input <?php
					if (get_option( 'la_sentinelle-wplogin', 'true') == 'true') {
						echo 'checked="checked"';
					} ?>
					type="checkbox" name="la_sentinelle-wplogin" id="la_sentinelle-wplogin">
					<label for="la_sentinelle-wplogin">
						<?php esc_html_e('Enable spamfilters for WordPress Login Form.', 'la-sentinelle-antispam'); ?>
					</label><br /><br />

					<input <?php
					if (get_option( 'la_sentinelle-wppassword', 'true') == 'true') {
						echo 'checked="checked"';
					} ?>
					type="checkbox" name="la_sentinelle-wppassword" id="la_sentinelle-wppassword">
					<label for="la_sentinelle-wppassword">
						<?php esc_html_e('Enable spamfilters for WordPress Reset Password Form.', 'la-sentinelle-antispam'); ?>
					</label><br /><br />

					<input <?php
					if (get_option( 'la_sentinelle-wpregister', 'true') == 'true') {
						echo 'checked="checked"';
					} ?>
					type="checkbox" name="la_sentinelle-wpregister" id="la_sentinelle-wpregister">
					<label for="la_sentinelle-wpregister">
						<?php esc_html_e('Enable spamfilters for WordPress Register Form.', 'la-sentinelle-antispam'); ?>
					</label><br /><br />

				</td>
			</tr>

		<?php
		$active_plugins = get_option('active_plugins');
		// print_r( $active_plugins );
		$supported_plugin = false;

		foreach ( $active_plugins as $plugin ) {

			if ( $plugin == 'caldera-forms/caldera-core.php' ) {
				$supported_plugin = true; ?>
				<tr valign="top">
					<th scope="row">
						<label for="la_sentinelle-caldera">Caldera Forms</label>
					</th>
					<td>
						<span class="setting-description">
							<?php
							esc_html_e( 'You can easily add the formfields for this plugin to your contact form. Just click the button for "Add Field". On the popup pane go to the Special tab and add the form-tag for La Sentinelle to the form.', 'la-sentinelle-antispam' ); echo '<br />';
							esc_html_e( 'You need to give in a name/slug, which can be anything.', 'la-sentinelle-antispam' ); echo '<br />';
							$link = '<a href="' . admin_url( 'admin.php?page=caldera-forms' ) . '">' . esc_html__( 'caldera forms overview', 'la-sentinelle-antispam' ).'</a>';
							/* translators: %s is a link to the contact forms overview. */
							echo sprintf( esc_html__( 'You can go to the %s and add it to the forms that you want.', 'la-sentinelle-antispam' ), $link );
							?>
						</span>
					</td>
				</tr><?php
			}

			if ( $plugin == 'contact-form-7/wp-contact-form-7.php' ) {
				$supported_plugin = true; ?>
				<tr valign="top">
					<th scope="row">
						<label for="la_sentinelle-cf7">Contact Form 7</label>
					</th>
					<td>
						<span class="setting-description">
							<?php /* translators: form-tag is a term specific to Contact Form 7, it is a shortcode-like tag in the form. */
							esc_html_e( 'You can easily add the formfields for this plugin to your contact form. Just click the button for "La Sentinelle" and on the popup pane add the tag to the form.', 'la-sentinelle-antispam' ); echo '<br />';
							$link = '<a href="' . admin_url( 'admin.php?page=wpcf7' ) . '">' . esc_html__( 'contact form 7 overview', 'la-sentinelle-antispam' ).'</a>';
							/* translators: %s is a link to the contact forms overview. */
							echo sprintf( esc_html__( 'You can go to the %s and add it to the forms that you want.', 'la-sentinelle-antispam' ), $link );
							?>
						</span>
					</td>
				</tr><?php
			}

			if ( $plugin == 'easy-digital-downloads/easy-digital-downloads.php' ) {
				$supported_plugin = true; ?>
				<tr valign="top">
					<th scope="row">
						Easy Digital Downloads
					</th>
					<td>
						<span class="setting-description">
							<?php
							esc_html_e( 'Login form and Register form in Easy Digital Downloads are supported automatically.', 'la-sentinelle-antispam' ); echo '<br />';
							esc_html_e( 'Please check the checkboxes for WordPress forms above.', 'la-sentinelle-antispam' ); echo '<br />';
							?>
						</span>
					</td>
				</tr><?php
			}

			if ( $plugin == 'formidable/formidable.php' ) {
				$supported_plugin = true; ?>
				<tr valign="top">
					<th scope="row">
						<label for="la_sentinelle-formidable">Formidable</label>
					</th>
					<td>
						<input <?php
						if (get_option( 'la_sentinelle-formidable', 'true') == 'true') {
							echo 'checked="checked"';
						} ?>
						type="checkbox" name="la_sentinelle-formidable" id="la_sentinelle-formidable">
						<label for="la_sentinelle-formidable">
							<?php esc_html_e('Enable spamfilters for Formidable.', 'la-sentinelle-antispam'); ?>
						</label><br />
						<span class="setting-description">
							<?php
							esc_html_e( 'Enabling this setting will add La Sentinelle antispam fields to every Formidable form.', 'la-sentinelle-antispam' ); echo '<br />';
							?>
						</span>
					</td>
				</tr><?php
			}

			if ( $plugin == 'woocommerce/woocommerce.php' ) {
				$supported_plugin = true; ?>
				<tr valign="top">
					<th scope="row">
						WooCommerce
					</th>
					<td>
						<span class="setting-description">
							<?php
							esc_html_e( 'Login form and Lost Password form in WooCommerce are supported automatically.', 'la-sentinelle-antispam' ); echo '<br />';
							esc_html_e( 'Please check the checkboxes for WordPress forms above.', 'la-sentinelle-antispam' ); echo '<br />';
							?>
						</span>
					</td>
				</tr><?php
			}

		}

		if ( $supported_plugin == false ) { ?>
				<tr valign="top">
					<th scope="row">
						<?php esc_html_e('No Plugins', 'la-sentinelle-antispam'); ?>
					</th>
					<td>
						<span class="setting-description">
							<?php
							esc_html_e( "You don't have any plugins installed that use the antispam features of La Sentinelle.", 'la-sentinelle-antispam' );
							?>
						</span>
					</td>
				</tr><?php
		}
		?>

		<tr>
			<th colspan="2">
				<p class="submit">
					<input type="submit" name="la_sentinelle_settings_submit" id="la_sentinelle_settings_submit" class="button-primary" value="<?php esc_attr_e('Save settings', 'la-sentinelle-antispam'); ?>" />
				</p>
			</th>
		</tr>

		</tbody>
	</table>

	<?php
}
