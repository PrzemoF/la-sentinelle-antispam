<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * About tab of the Settings page.
 */
function la_sentinelle_settingstab_about() {
	?>
	<table class="form-table">
		<tbody>

		<tr valign="top">
			<th><?php esc_html_e('Statistics', 'la-sentinelle-antispam'); ?></th>
			<td>
				<p><?php
					$active_plugins = get_option('active_plugins');

					echo '<b>' . esc_html__('WordPress forms:', 'la-sentinelle-antispam') . '</b><br />';

					$wpcomments_blocked = la_sentinelle_get_statistic_blocked( 'wpcomments' );
					/* translators: %d is a counter for comments blocked */
					echo sprintf( esc_html__( '%d spam comments were blocked.', 'la-sentinelle-antispam' ), $wpcomments_blocked ) . '<br />';

					$wplogin_blocked = la_sentinelle_get_statistic_blocked( 'wplogin' );
					/* translators: %d is a counter for login tries blocked */
					echo sprintf( esc_html__( '%d login tries were blocked.', 'la-sentinelle-antispam' ), $wplogin_blocked ) . '<br />';

					$wpregister_blocked = la_sentinelle_get_statistic_blocked( 'wpregister' );
					/* translators: %d is a counter for register tries blocked */
					echo sprintf( esc_html__( '%d register tries were blocked.', 'la-sentinelle-antispam' ), $wpregister_blocked ) . '<br />';

					$wppassword_blocked = la_sentinelle_get_statistic_blocked( 'wppassword' );
					/* translators: %d is a counter for password reset tries blocked */
					echo sprintf( esc_html__( '%d password reset tries were blocked.', 'la-sentinelle-antispam' ), $wppassword_blocked ) . '<br />';

					if ( in_array( 'caldera-forms/caldera-core.php', $active_plugins ) ) {
						echo '<br /><b>' . esc_html__('Caldera Forms:', 'la-sentinelle-antispam') . '</b><br />';

						$caldera_blocked = la_sentinelle_get_statistic_blocked( 'caldera' );
						/* translators: %d is a counter for form submissions blocked */
						echo sprintf( esc_html__( '%d form submissions were blocked.', 'la-sentinelle-antispam' ), $caldera_blocked ) . '<br />';
					}

					if ( in_array( 'contact-form-7/wp-contact-form-7.php', $active_plugins ) ) {
						echo '<br /><b>' . esc_html__('Contact Form 7:', 'la-sentinelle-antispam') . '</b><br />';

						$cf7_blocked = la_sentinelle_get_statistic_blocked( 'cf7' );
						/* translators: %d is a counter for form submissions blocked */
						echo sprintf( esc_html__( '%d form submissions were blocked.', 'la-sentinelle-antispam' ), $cf7_blocked ) . '<br />';
					}

					if ( in_array( 'formidable/formidable.php', $active_plugins ) ) {
						echo '<br /><b>' . esc_html__('Formidable:', 'la-sentinelle-antispam') . '</b><br />';

						$formidable_blocked = la_sentinelle_get_statistic_blocked( 'formidable' );
						/* translators: %d is a counter for form submissions blocked */
						echo sprintf( esc_html__( '%d form submissions were blocked.', 'la-sentinelle-antispam' ), $formidable_blocked ) . '<br />';
					}
					?>
				</p>
			</td>
		</tr>

		<tr valign="top">
			<th><?php esc_html_e('Support', 'la-sentinelle-antispam'); ?></th>
			<td>
				<p><?php
					$support = '<a href="https://wordpress.org/support/plugin/la-sentinelle-antispam" target="_blank">';
					/* translators: %s is a link */
					echo sprintf( esc_html__( 'If you have a problem or a feature request, please post it on the %ssupport forum at wordpress.org%s.', 'la-sentinelle-antispam' ), $support, '</a>' ); ?>
					<?php esc_html_e('I will do my best to respond as soon as possible.', 'la-sentinelle-antispam'); ?><br />
					<?php esc_html_e('If you send me an email, I will not reply. Please use the support forum.', 'la-sentinelle-antispam'); ?>
				</p>
			</td>
		</tr>

		<tr valign="top">
			<th><?php esc_html_e('Review', 'la-sentinelle-antispam'); ?></th>
			<td>
				<p><?php
					$review = '<a href="https://wordpress.org/support/view/plugin-reviews/la-sentinelle-antispam?rate=5#postform" target="_blank">';
					/* translators: %s is a link */
					echo sprintf( esc_html__( 'If this plugin has any value to you, then please leave a review at %sthe plugin page%s at wordpress.org.', 'la-sentinelle-antispam' ), $review, '</a>' ); ?>
				</p>
			</td>
		</tr>

		<tr valign="top">
			<th><?php esc_html_e('Translations', 'la-sentinelle-antispam'); ?></th>
			<td>
				<p><?php
					$link = '<a href="https://translate.wordpress.org/projects/wp-plugins/la-sentinelle-antispam" target="_blank">';
					/* translators: %s is a link */
					echo sprintf( esc_html__( 'Translations can be added very easily through %sGlotPress%s.', 'la-sentinelle-antispam' ), $link, '</a>' ); echo '<br />';
					echo sprintf( esc_html__( "You can start translating strings there for your locale. They need to be validated though, so if there's no validator yet, and you want to apply for being validator (PTE), please post it on the %ssupport forum%s.", 'la-sentinelle-antispam' ), $support, '</a>' ); echo '<br />';
					$make = '<a href="https://make.wordpress.org/polyglots/" target="_blank">';
					/* translators: %s is a link */
					echo sprintf( esc_html__( 'I will make a request on %smake/polyglots%s to have you added as validator for this plugin/locale.', 'la-sentinelle-antispam' ), $make, '</a>' ); ?>
				</p>
			</td>
		</tr>

		</tbody>
	</table>

	<?php
}
