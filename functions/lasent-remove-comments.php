<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Auto Deletes comments that are older than 3 months.
 *
 * @since 1.0.2
 */
function la_sentinelle_remove_comments() {

	if (get_option( 'la_sentinelle-remove_comments', 'false') == 'true') {

		$current_time = current_time('timestamp');
		$timestamp = strtotime( '-3 months', $current_time );

		$year = strftime( '%Y', $timestamp );
		$month = strftime( '%m', $timestamp );
		$day = strftime( '%d', $timestamp );

		$args = array(
			'fields'     => 'ids',
			'number'     => 10,
			'orderby'    => 'comment_date_gmt',
			'order'      => 'ASC',
			'status'     => 'spam',
			'date_query' => array(
					array(
						'before'    => array(
							'year'  => $year,
							'month' => $month,
							'day'   => $day,
						),
					),
				),
			);
		$comments = get_comments( $args );

		if ( is_array($comments) && ! empty($comments) ) {
			foreach ($comments as $comment_id) {
				wp_delete_comment( $comment_id, true );
			}
		}
	}
}
if (get_option( 'la_sentinelle-wpcomment', 'true') == 'true') {
	add_action( 'shutdown', 'la_sentinelle_remove_comments' );
}
