<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Add spamfilter fields to lost password form.
 *
 * @since 1.0.0
 */
function la_sentinelle_lostpassword_form() {

	echo la_sentinelle_get_spamfilters();

}
if (get_option( 'la_sentinelle-wppassword', 'true') == 'true') {
	// Add spamfilter fields to WordPress lost password form.
	add_action( 'lostpassword_form', 'la_sentinelle_lostpassword_form' );
	add_action( 'lostpassword_form', 'la_sentinelle_dead_enqueue' );

	// Add spamfilter fields to WooCommerce lost password form.
	add_action( 'woocommerce_lostpassword_form', 'la_sentinelle_lostpassword_form' );
}


/*
 * Check fields in lost password form.
 *
 * @param  array errors
 *
 * @since 1.0.0
 */
function la_sentinelle_lostpassword_post( $errors ) {

	if ( defined('XMLRPC_REQUEST') && XMLRPC_REQUEST ) {
		return;
	}

	$marker_nonce = la_sentinelle_check_nonce();
	if ( $marker_nonce == 'spam' ) {
		$errors->add( 'likely_spammer', esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}
	$marker_honeypot = la_sentinelle_check_honeypot();
	if ( $marker_honeypot == 'spam' ) {
		$errors->add( 'likely_spammer', esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) );
	}
	$marker_timeout = la_sentinelle_check_timeout();
	if ( $marker_timeout == 'spam' ) {
		$errors->add( 'likely_spammer', esc_html__( 'Your submission was sent in too fast. Please slow down and try again.', 'la-sentinelle-antispam' ) );
	}

	if ( $marker_nonce == 'spam' || $marker_honeypot == 'spam' || $marker_timeout == 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'wppassword' );
	}

	return $errors;

}
if (get_option( 'la_sentinelle-wppassword', 'true') == 'true') {
	// Check fields in lost password form for WordPress.
	add_action( 'lostpassword_post', 'la_sentinelle_lostpassword_post', 10 );

	// Check fields in lost password form for Woocommerce.
	// Already done in lostpassword_post action.
	//add_filter( 'allow_password_reset', 'la_sentinelle_lostpassword_post', 10, 1 );


}
